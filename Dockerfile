FROM		mysql:5.5

COPY 		docker-entrypoint.sh /entrypoint.sh
ENTRYPOINT 	["/entrypoint.sh"]

EXPOSE 3306
CMD 		["mysqld"]

ENV		MYSQL_LOWER_CASE_TABLE_NAMES		1

